import React from 'react';
import './Board.css';


const Square = props => {
    return (
      <button onClick={props.onClick} className="square">
        {props.value}
      </button>
    );
};

class Board extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          squares: Array(9).fill(null),
          isNext: true,
        };
      }

      handleClick(i) {
        const squares = this.state.squares.slice();
        if (checkWinnerPossibilities(squares) || squares[i]) {
          return;
        }
        squares[i] = this.state.isNext ? 'X' : 'O';
        this.setState({
          squares: squares,
          isNext: !this.state.isNext,
        });
      }
      resetGame() {
        this.setState({
            squares: Array(9).fill(null),
            isNext: true
        });
      }

      
    squareHandler(i) {
      return (
        <Square
        value={this.state.squares[i]}
        onClick={() => this.handleClick(i)}
      />
      );
    }
  
    render() {
        const result = checkWinnerPossibilities(this.state.squares);
        let nextPlayer;
        let winner;
        if (result) {
        winner =  result;
        } else {
            nextPlayer = (this.state.isNext ? 'X' : 'O');
        }
      return (
        
        <div className="squareGrid">
            <div className="status">Next Player: {nextPlayer}</div>
            <div className="status"> Winner: {winner}</div>
            
            <div className="row">
                {this.squareHandler(0)}
                {this.squareHandler(1)}
                {this.squareHandler(2)}
            </div>
            <div className="row">
                {this.squareHandler(3)}
                {this.squareHandler(4)}
                {this.squareHandler(5)}
            </div>
            <div className="row">
                {this.squareHandler(6)}
                {this.squareHandler(7)}
                {this.squareHandler(8)}
            </div>
            <button onClick={() => this.resetGame()}>Reset Game</button>
        </div> 
      );
    }
}


// function to check winner conditions
function checkWinnerPossibilities(arr) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (arr[a] && arr[a] === arr[b] && arr[a] === arr[c]) {
        return arr[a];
      }
    }
    return null;
  }
  

export default Board;